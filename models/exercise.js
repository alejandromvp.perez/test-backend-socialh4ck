var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var exerciseSchema = new Schema({
    name:               { type: String },
    typeExercise:       { type: String },
    mainImage:          { type: String },
    levelDifficulty:    { type: Number },
    timer:              { duration: { type: Number } },
    lights: [{
        colorCode: { type: String }
    }],
    music: [{
        name: { type: String },
        sourcePath: { type: String },
        volumen: { type: Number }
    }],
    videoTutor: [{
        name: { type: String },
        sourcePath: { type: String }
    }],
    createdBy:          { type: String },
    status:             { type: String, enum: ['enabled', 'disable'] }
});

module.exports = mongoose.model('Excercise', exerciseSchema);