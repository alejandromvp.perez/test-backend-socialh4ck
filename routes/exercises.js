module.exports = function (app) {

    var Excercise = require('../models/exercise.js');

    //GET ALL
    readAll = function (req, res) {
        Excercise.find(function (err, excercises) {
            if (!err) {
                console.log('GET ALL /excercises')
                res.send(excercises);
            } else {
                console.log('ERROR: ' + err);
            }
        });
    };

    //GET 
    readById = function (req, res) {
        Excercise.findById(req.params.id, function (err, excercise) {
            if (!err) {
                console.log('GET /excercise/' + req.params.id);
                res.send(excercise);
            } else {
                console.log('ERROR: ' + err);
            }
        });
    };

    //POST
    createExcercise = function (req, res) {
        console.log('POST');
        console.log(req.body);

        var excercise = new Excercise({
            name: req.body.name,
            typeExercise: req.body.typeExercise,
            mainImage: req.body.mainImage,
            levelDifficulty: req.body.levelDifficulty,
            timer: req.body.timer,
            lights: req.body.lights,
            music: req.body.music,
            videoTutor: req.body.videoTutor,
            createdBy: req.body.createdBy,
            status: req.body.status
        });

        excercise.save(function (err) {
            if (!err) {
                console.log('Created');
            } else {
                console.log('ERROR: ' + err);
            }
        });

        res.send(excercise);
    };

    //PUT 
    updateExcercise = function (req, res) {
        Excercise.findById(req.params.id, function (err, excercise) {
            excercise.name = req.body.name;
            excercise.typeExercise = req.body.typeExercise;
            excercise.mainImage = req.body.mainImage;
            excercise.levelDifficulty = req.body.levelDifficulty;
            excercise.timer = req.body.timer;
            excercise.lights = req.body.lights;
            excercise.music = req.body.music;
            excercise.videoTutor = req.body.videoTutor;
            excercise.createdBy = req.body.createdBy;
            excercise.status = req.body.status;

            excercise.save(function (err) {
                if (!err) {
                    console.log('Updated');
                } else {
                    console.log('ERROR: ' + err);
                }
                res.send(excercise);
            });
        });
    }

    //DELETE
    deleteExcercise = function (req, res) {
        Excercise.findById(req.params.id, function (err, excercise) {
            excercise.remove(function (err) {
                if (!err) {
                    console.log('Removed');
                    res.send(true);
                } else {
                    console.log('ERROR: ' + err);
                }
            })
        });
    }

    app.get('/excercises', readAll);
    app.get('/excercise/:id', readById);
    app.post('/excercise', createExcercise);
    app.put('/excercise/:id', updateExcercise);
    app.delete('/excercise/:id', deleteExcercise);

}